/** @file */
#ifndef __ARRAYLIST_H
#define __ARRAYLIST_H

#include "IndexOutOfBound.h"
#include "ElementNotExist.h"

/**
 * The ArrayList is just like vector in C++.
 * You should know that "capacity" here doesn't mean how many elements are now in this list, where it means
 * the length of the array of your internal implemention
 *
 * The iterator iterates in the order of the elements being loaded into this list
 */
template<class T>
class ArrayList {
private:
	int maxSize, currentLength;
	T* seq;

	void doublespace() {
		T* tmp = seq;
		seq = new T[maxSize << 1];
		for (int i = 0; i < maxSize; ++i)
			seq[i] = tmp[i];
		maxSize <<= 1;
		delete[] tmp;
	}

public:
	class Iterator {
	private:
		int now;
		int last;
		ArrayList *u;
	public:
		/**
		 * TODO Returns true if the iteration has more elements.
		 */
		bool hasNext() {
			if (now == u->size() - 1) return false; else return true;
		}

		/**
		 * TODO Returns the next element in the iteration.
		 * @throw ElementNotExist exception when hasNext() == false
		 */
		const T &next() {
			if (!hasNext()) throw ElementNotExist();
			++now;
			last = now;
			return u->get(now);
		}

		/**
		 * TODO Removes from the underlying collection the last element
		 * returned by the iterator
		 * The behavior of an iterator is unspecified if the underlying
		 * collection is modified while the iteration is in progress in
		 * any way other than by calling this method.
		 * @throw ElementNotExist
		 */
		void remove() {
			if (last == -1) throw ElementNotExist();
			u->removeIndex(last);
			--now;
			last = -1;
		}
		
		void initialize(ArrayList *obj){
			last = -1;
			now = -1;
			u = obj;
		}
	};

	/**
	 * TODO Constructs an empty array list.
	 */
	ArrayList() {
		maxSize = 1;
		currentLength = 0;
		seq = new T[1];
	}

	/**
	 * TODO Destructor
	 */
	~ArrayList() {
		delete[] seq;
	}

	/**
	 * TODO Assignment operator
	 */
	ArrayList& operator=(const ArrayList& x) {
		if (&x == this) return *this;
		delete[] seq;
		maxSize = currentLength = x.size();
		if (maxSize == 0) maxSize = 1;
		seq = new T[maxSize];
		for (int i = 0; i < currentLength; ++i)
			seq[i] = x.get(i);
		return *this;
	}

	/**
	 * TODO Copy-constructor
	 */
	ArrayList(const ArrayList& x) {
		maxSize = currentLength = x.size();
		if (maxSize < 0) maxSize = 1;
		seq = new T[maxSize];
		for (int i = 0; i < currentLength; ++i)
			seq[i] = x.get(i);
	}

	/**
	 * TODO Appends the specified element to the end of this list.
	 * Always returns true.
	 */
	bool add(const T& e) {
		if (currentLength == maxSize)
			doublespace();
		++currentLength;
		seq[currentLength - 1] = e;
		return true;
	}

	/**
	 * TODO Inserts the specified element to the specified position in this list.
	 * The range of index parameter is [0, size], where index=0 means inserting to the head,
	 * and index=size means appending to the end.
	 * @throw IndexOutOfBound
	 */
	void add(int index, const T& element) {
		if (index > currentLength || index < 0)
			throw IndexOutOfBound();
		if (currentLength == maxSize)
			doublespace();
		++currentLength;
		for (int i = currentLength - 1; i > index; --i)
			seq[i] = seq[i - 1];
		seq[index] = element;
	}

	/**
	 * TODO Removes all of the elements from this list.
	 */
	void clear() {
		currentLength = 0;
	}

	/**
	 * TODO Returns true if this list contains the specified element.
	 */
	bool contains(const T& e) const {
		bool found = false;
		for (int i = 0; i < currentLength; ++i)
			if (seq[i] == e) {
				found = true;
				break;
			}
		return found;
	}

	/**
	 * TODO Returns a const reference to the element at the specified position in this list.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	const T& get(int index) const {
		if (index >= currentLength || index < 0)
			throw IndexOutOfBound();
		return seq[index];
	}

	/**
	 * TODO Returns true if this list contains no elements.
	 */
	bool isEmpty() const {
		if (currentLength)
			return false;
		else
			return true;
	}

	/**
	 * TODO Removes the element at the specified position in this list.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	void removeIndex(int index) {
		if (index >= currentLength || index < 0)
			throw IndexOutOfBound();
		for (int i = index; i < currentLength - 1; ++i)
			seq[i] = seq[i + 1];
		--currentLength;
	}

	/**
	 * TODO Removes the first occurrence of the specified element from this list, if it is present.
	 * Returns true if it was present in the list, otherwise false.
	 */
	bool remove(const T &e) {
		int found = -1;
		for (int i = 0; i < currentLength; ++i) 
			if (seq[i] == e){
				found = i;
				break;
			}
		if (found == -1)
			return false;
		for (int i = found; i < currentLength - 1; ++i)
			seq[i] = seq[i + 1];
		--currentLength;
		return true;
	}

	/**
	 * TODO Replaces the element at the specified position in this list with the specified element.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	void set(int index, const T &element) {
		if (index >= currentLength || index < 0)
			throw IndexOutOfBound();
		seq[index] = element;
	}

	/**
	 * TODO Returns the number of elements in this list.
	 */
	int size() const {
		return currentLength;
	}

	/**
	 * TODO Returns an iterator over the elements in this list.
	 */
	Iterator iterator() {
		Iterator t;
		t.initialize(this);
		return t;
	}
};

#endif
