/** @file */
#ifndef __DEQUE_H
#define __DEQUE_H

#include "ElementNotExist.h"
#include "IndexOutOfBound.h"

/**
 * An deque is a linear collection that supports element insertion and removal at both ends.
 * The name deque is short for "double ended queue" and is usually pronounced "deck".
 * Remember: all functions but "contains" and "clear" should be finished in O(1) time.
 *
 * You need to implement both iterators in proper sequential order and ones in reverse sequential order. 
 */
template<class T>
class Deque {
private:
	
	int l, r, maxSize;
	T *seq;
	
	void doubleSpace(){
		T *t = seq;
		seq = new T[maxSize * 2 + 1];
		int L = maxSize - (r - l + 1) / 2, R = L + r - l;
		for (int i = L; i <= R; ++i)
			seq[i] = t[l + i - L];
		l = L;
		r = R;
		maxSize = maxSize * 2 + 1;
		delete[] t;
	}
	
public:
	class Iterator {
		Deque *u;
		bool order;
		int now, last;
	public:
		
		Iterator(Deque *_u, bool _order){
			u = _u;
			order = _order;
			if (order){
				now = -1;
				last = -1;
			} else {
				now = u->size();
				last = -1;
			}
		}
		
		/**
		 * TODO Returns true if the iteration has more elements.
		 */
		bool hasNext() {
			if (order){
				if (now == u->size() - 1) return false; else return true;
			} else {
				if (now == 0) return false; else return true;
			}
		}

		/**
		 * TODO Returns the next element in the iteration.
		 * @throw ElementNotExist exception when hasNext() == false
		 */
		const T &next() {
			if (!hasNext()) throw ElementNotExist();
			if (order){
				++now;
				last = now;
				return u->get(now);
			} else {
				--now;
				last = now;
				return u->get(now);
			}
		}

		/**
		 * TODO Removes from the underlying collection the last element
		 * returned by the iterator
		 * The behavior of an iterator is unspecified if the underlying
		 * collection is modified while the iteration is in progress in
		 * any way other than by calling this method.
		 * @throw ElementNotExist
		 */
		void remove() {
			if (order){
				if (last == -1) throw ElementNotExist();
				u->remove(last);
				last = -1;
				--now;
			} else {
				if (last == -1) throw ElementNotExist();
				u->remove(last);
				last = -1;
			}
		}
	};

	/**
	 * TODO Constructs an empty deque.
	 */
	Deque() {
		maxSize = 3;
		seq = new T[maxSize];
		l = 1;
		r = 0;
	}

	/**
	 * TODO Destructor
	 */
	~Deque() {
		delete[] seq;
	}

	/**
	 * TODO Assignment operator
	 */
	Deque& operator=(const Deque& x) {
		if (&x == this) return *this;
		delete[] seq;
		maxSize = x.size();
		l = 0;
		r = maxSize - 1;
		seq = new T[maxSize];
		for (int i = l; i <= r; ++i)
			seq[i] = x.get(i);		
		return *this;
	}

	/**
	 * TODO Copy-constructor
	 */
	Deque(const Deque& x) {
		maxSize = x.size();
		l = 0;
		r = maxSize - 1;
		seq = new T[maxSize];
		for (int i = l; i <= r; ++i)
			seq[i] = x.get(i);
	}

	/**
	 * TODO Inserts the specified element at the front of this deque. 
	 */
	void addFirst(const T& e) {
		if (l == 0) doubleSpace();
		seq[--l] = e;
	}

	/**
	 * TODO Inserts the specified element at the end of this deque.
	 */
	void addLast(const T& e) {
		if (r == maxSize - 1) doubleSpace();
		seq[++r] = e;
	}

	/**
	 * TODO Returns true if this deque contains the specified element.
	 */
	bool contains(const T& e) const {
		bool found = false;
		for (int i = l; i <= r; ++i)
			if (seq[i] == e){
				found = true;
				break;
			}
		return found;
	}

	/**
	 * TODO Removes all of the elements from this deque.
	 */
	void clear() {
		l = maxSize / 2;
		r = l - 1;
	}

	/**
	 * TODO Returns true if this deque contains no elements.
	 */
	bool isEmpty() const {
		if (l > r) return true; else return false;
	}

	/**
	 * TODO Retrieves, but does not remove, the first element of this deque.
	 * @throw ElementNotExist
	 */
	const T& getFirst() {
		if (l > r) throw ElementNotExist();
		return seq[l];
	}

	/**
	 * TODO Retrieves, but does not remove, the last element of this deque.
	 * @throw ElementNotExist
	 */
	const T& getLast() {
		if (l > r) throw ElementNotExist();
		return seq[r];
	}
	
	void remove(int index){
		if (index < 0 || index >= r - l + 1) throw IndexOutOfBound();
		for (int i = l + index; i < r; ++i) seq[i] = seq[i + 1];
		--r;
	}

	/**
	 * TODO Removes the first element of this deque.
	 * @throw ElementNotExist
	 */
	void removeFirst() {
		if (l > r) throw ElementNotExist();
		++l;
	}

	/**
	 * TODO Removes the last element of this deque.
	 * @throw ElementNotExist
	 */
	void removeLast() {
		if (l > r) throw ElementNotExist();
		--r;
	}

	/**
	 * TODO Returns a const reference to the element at the specified position in this deque.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	const T& get(int index) const {
		if (index < 0 || index >= r - l + 1) throw IndexOutOfBound();
		return seq[l + index];
	}

	/**
	 * TODO Replaces the element at the specified position in this deque with the specified element.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	void set(int index, const T& e) {
		if (index < 0 || index >= r - l + 1) throw IndexOutOfBound();
		seq[l + index] = e;
	}

	/**
	 * TODO Returns the number of elements in this deque.
	 */
	int size() const {
		return r - l + 1;
	}

	/**
	 * TODO Returns an iterator over the elements in this deque in proper sequence.
	 */
	Iterator iterator() {
		Iterator t = Iterator(this, true);
		return t;
	}

	/**
	 * TODO Returns an iterator over the elements in this deque in reverse sequential order.
	 */
	Iterator descendingIterator() {
		Iterator t = Iterator(this, false);
		return t;
	}
	
	int getL(){
		return l;
	}
	
	int getR(){
		return r;
	}
	
	int getSize(){
		return maxSize;
	}
};

#endif
