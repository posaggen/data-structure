/** @file */
#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H

#include "IndexOutOfBound.h"
#include "ElementNotExist.h"

/**
 * A linked list.
 *
 * The iterator iterates in the order of the elements being loaded into this list.
 */
template<class T>
class LinkedList {
private:
	
	class Node{
	public:
		T x;
		Node *next, *pre;
		Node(T _x):x(_x){
			next = pre = NULL;
		}
	};
	
	int Size;
	Node *head, *tail;
	
public:
	class Iterator {
	private:
		LinkedList *u;
		int now;
		int last;
	public:
		Iterator(LinkedList *t){
			u = t;
			now = last = -1;
		}
		/**
		 * TODO Returns true if the iteration has more elements.
		 */
		bool hasNext() {
			if (now == u->size() - 1) return false; else return true;
		}

		/**
		 * TODO Returns the next element in the iteration.
		 * @throw ElementNotExist exception when hasNext() == false
		 */
		const T &next() {
			if (!hasNext()) throw ElementNotExist();
			++now;
			last = now;
			return u->get(now);
		}
		
		/**
		 * TODO Removes from the underlying collection the last element
		 * returned by the iterator
		 * The behavior of an iterator is unspecified if the underlying
		 * collection is modified while the iteration is in progress in
		 * any way other than by calling this method.
		 * @throw ElementNotExist
		 */
		void remove() {
			if (last == -1) throw ElementNotExist();
			u->removeIndex(last);
			--now;
			last = -1;
		}
	};

	/**
	 * TODO Constructs an empty linked list
	 */
	LinkedList() {
		Size = 0;
		head = tail = NULL;
	}

	/**
	 * TODO Copy constructor
	 */
	LinkedList(const LinkedList<T> &c) {
		Size = c.size();
		head = tail = NULL;
		if (Size){
			head = new Node(c.getFirst());
			tail = head;
			for (int i = 1; i < Size; ++i){
				Node *t = new Node(c.get(i));
				tail->next = t;
				t->pre = tail;
				tail = t;
			}
		}
	}

	/**
	 * TODO Assignment operator
	 */
	LinkedList<T>& operator=(const LinkedList<T> &c) {
		if (&c == this) return *this;
		clear();
		Size = c.size();
		head = tail = NULL;
		if (Size){
			head = new Node(c.getFirst());
			tail = head;
			for (int i = 1; i < Size; ++i){
				Node *t = new Node(c.get(i));
				tail->next = t;
				t->pre = tail;
				tail = t;
			}
		}
		return *this;
	}

	/**
	 * TODO Desturctor
	 */
	~LinkedList() {
		clear();
	}

	/**
	 * TODO Appends the specified element to the end of this list.
	 * Always returns true.
	 */
	bool add(const T& e) {
		addLast(e);
		return true;
	}

	/**
	 * TODO Inserts the specified element to the beginning of this list.
	 */
	void addFirst(const T& elem) {
		Node *t = new Node(elem);
		if (!Size){
			head = tail = t;
			++Size;
			return;
		}
		head->pre = t;
		t->next = head;
		head = t;
		++Size;
	}

	/**
	 * TODO Insert the specified element to the end of this list.
	 * Equivalent to add.
	 */
	void addLast(const T &elem) {
		Node *t = new Node(elem);
		if (!Size){
			head = tail = t;
			++Size;
			return;
		}
		tail->next = t;
		t->pre = tail;
		tail = t;
		++Size;
	}

	/**
	 * TODO Inserts the specified element to the specified position in this list.
	 * The range of index parameter is [0, size], where index=0 means inserting to the head,
	 * and index=size means appending to the end.
	 * @throw IndexOutOfBound
	 */
	void add(int index, const T& element) {
		if (index < 0 || index > Size) throw IndexOutOfBound();
		Node *pre = NULL, *u = head;
		for (int i = 0; i < index; ++i){
			pre = u;
			u = u->next;
		}
		Node *t = new Node(element);
		if (pre) pre->next = t;
		if (u) u->pre = t;
		t->pre = pre;
		t->next = u;
		if (index == 0) head = t;
		if (index == Size) tail = t;
		++Size;
	}

	/**
	 * TODO Removes all of the elements from this list.
	 */
	void clear() {
		Size = 0;
		while (head){
			Node *u = head;
			head = head->next;
			delete u;
		}
		head = tail = NULL;
	}

	/**
	 * TODO Returns true if this list contains the specified element.
	 */
	bool contains(const T& e) const {
		Node *u = head;
		bool found = false;
		while (u){
			if (u->x == e){
				found = true;
				break;
			}
			u = u->next;
		}
		return found;
	}

	/**
	 * TODO Returns a const reference to the element at the specified position in this list.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	const T& get(int index) const {
		if (index < 0 || index >= Size) throw IndexOutOfBound();
		Node *u = head;
		for (int i = 0; i < index; ++i) u = u->next;
		return u->x;
	}

	/**
	 * TODO Returns a const reference to the first element.
	 * @throw ElementNotExist
	 */
	const T& getFirst() const {
		if (!head) throw ElementNotExist();
		return head->x;
	}

	/**
	 * TODO Returns a const reference to the last element.
	 * @throw ElementNotExist
	 */
	const T& getLast() const {
		if (!tail) throw ElementNotExist();
		return tail->x;
	}

	/**
	 * TODO Returns true if this list contains no elements.
	 */
	bool isEmpty() const {
		if (!Size) return true; else return false;
	}

	/**
	 * TODO Removes the element at the specified position in this list.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	void removeIndex(int index) {
		if (index < 0 || index >= Size) throw IndexOutOfBound();
		Node *u = head;
		for (int i = 0; i < index; ++i) u = u->next;
		if (u->pre) u->pre->next = u->next;
		if (u->next) u->next->pre = u->pre;
		if (index == 0) head = head->next;
		if (index == Size - 1) tail = tail->pre;
		--Size;
		if (Size == 0){
			head = tail = NULL;
		}
		delete u;
	}

	/**
	 * TODO Removes the first occurrence of the specified element from this list, if it is present.
	 * Returns true if it was present in the list, otherwise false.
	 */
	bool remove(const T &e) {
		Node *u = head;
		while (u && u->x != e) u = u->next;
		if (!u) return false;
		if (u->pre) u->pre->next = u->next;
		if (u->next) u->next->pre = u->pre;
		--Size;
		if (u == head) head = head->next;
		if (u == tail) tail = tail->pre;
		if (Size == 0){
			head = tail = NULL;
		}
		delete u;
		return true;
	}

	/**
	 * TODO Removes the first element from this list.
	 * @throw ElementNotExist
	 */
	void removeFirst() {
		if (head == NULL) throw ElementNotExist();
		Node *t = head;
		head = head->next;
		if (head) head->pre = NULL;
		--Size;
		if (Size == 0){
			head = tail = NULL;
		}
		delete t;
	}

	/**
	 * TODO Removes the last element from this list.
	 * @throw ElementNotExist
	 */
	void removeLast() {
		if (tail == NULL) throw ElementNotExist();
		Node *t = tail;
		tail = tail->pre;
		if (tail) tail->next = NULL;
		--Size;
		if (Size == 0){
			head = tail = NULL;
		}
		delete t;
	}

	/**
	 * TODO Replaces the element at the specified position in this list with the specified element.
	 * The index is zero-based, with range [0, size).
	 * @throw IndexOutOfBound
	 */
	void set(int index, const T &element) {
		if (index < 0 || index >= Size) throw IndexOutOfBound();
		Node *u = head;
		for (int i = 0; i < index; ++i)
			u = u->next;
		u->x = element;
	}

	/**
	 * TODO Returns the number of elements in this list.
	 */
	int size() const {
		return Size;
	}

	/**
	 * TODO Returns an iterator over the elements in this list.
	 */
	Iterator iterator() {
		Iterator t = Iterator(this);
		return t;
	}
};

#endif
