/** @file */
#ifndef __PRIORITYQUEUE_H
#define __PRIORITYQUEUE_H

#include "ArrayList.h"
#include "ElementNotExist.h"

/**
 * This is a priority queue based on a priority priority queue. The
 * elements of the priority queue are ordered according to their 
 * natual ordering (operator<), or by a Comparator provided as the
 * second template parameter.
 * The head of this queue is the least element with respect to the
 * specified ordering (different from C++ STL).
 * The iterator does not return the elements in any particular order.
 * But it is required that the iterator will eventually return every
 * element in this queue (even if removals are performed).
 */

/*----------------------------------------------------------------------*/
/**
 * Default Comparator with respect to natural order (operator<).
 */
template<class V>
class Less {
public:
	bool operator()(const V& a, const V& b) {
		return a < b;
	}
};

/**
 * To use this priority queue, users need to either use the
 * default Comparator or provide their own Comparator of this
 * kind.
 * The Comparator should be a class with a public function
 * public: bool operator()(const V&, const V&);
 * overriding the "()" operator.
 * The following code may help you understand how to use a
 * Comparator and, especially, why we override operator().
 */

// #include <iostream>
// using namespace std;
// 
// template <class T, class C = Less<T> >
// class Example
// {
// private:
//     C cmp;
// public:
//     bool compare(const T& a, const T& b)
//     {
//         return cmp(a, b);
//     }
// };
// 
// int main()
// {
//     Example<int, Less<int> > example; // Less<int> can be omitted.
//     cout << example.compare(1, 2) << endl;
//     cout << example.compare(2, 1) << endl;
// }
/*----------------------------------------------------------------------*/

template<class V, class C = Less<V> >
class PriorityQueue {
private:
	C cmp;
	
	bool compare(V a, V b){
		return !cmp(a, b);
	}
	
	ArrayList<V> seq;
	
	int L(int t){
		return (t + 1) * 2 - 1;
	}
	
	int R(int t){
		return (t + 1) * 2;
	}
	
	int F(int t){
		if (t & 1) return (t + 1) / 2 - 1; else return t / 2 - 1;
	}
	
	void Swap(int x, int y){
		V tmp = seq.get(x);
		seq.set(x, seq.get(y));
		seq.set(y, tmp);
	}
	
	void down(int u){
		if (L(u) >= seq.size()) return;
		int t = u;
		if (compare(seq.get(u), seq.get(L(u)))) t = L(u);
		if (R(u) < seq.size() && compare(seq.get(t), seq.get(R(u)))) t = R(u);
		if (t != u){
			Swap(u, t);
			down(t);
		}
	}
	
	int up(int u){
		if (!u) return u;
		if (compare(seq.get(F(u)), seq.get(u))){
			Swap(F(u), u);
			return up(F(u));
		}
		return u;
	}
	
	int remove(int Index){
		Swap(Index, seq.size() - 1);
		seq.removeIndex(seq.size() - 1);
		if (Index < seq.size()){
			down(Index);
			int t = up(Index);
			if (t != Index) return t; else return Index;
		} else return -1;
	}
	
	void removeE(V t){
		for (int i = 0; i < seq.size(); ++i)
			if (compare(seq.get(i), t) && compare(t, seq.get(i))){
				remove(i);
				return;
			}
	}
	
public:
	class Iterator {
	private:
		PriorityQueue *u;
		int cur, last;
		bool lastE;
		V tmp;
		ArrayList<V> forgetMeNot;
	public:
		
		Iterator(PriorityQueue *t){
			u = t;
			cur = 0;
			last = -1;
			lastE = false;
			forgetMeNot.clear();
		}
		
		/**
		 * TODO Returns true if the iteration has more elements.
		 */
		bool hasNext() {
			return (cur < u->size() || !forgetMeNot.isEmpty());
		}

		/**
		 * TODO Returns the next element in the iteration.
		 * @throw ElementNotExist exception when hasNext() == false
		 */
		V next() {
			if (!hasNext()) throw ElementNotExist();
			if (cur < u->size()){
				last = cur;
				++cur;
				return u->getElement(last);
			} else {
				lastE = true;
				last = -1;
				tmp = forgetMeNot.get(forgetMeNot.size() - 1);
				forgetMeNot.removeIndex(forgetMeNot.size() - 1);
				return tmp;
			}
		}

		/**
		 * TODO Removes from the underlying collection the last element
		 * returned by the iterator.
		 * The behavior of an iterator is unspecified if the underlying
		 * collection is modified while the iteration is in progress in
		 * any way other than by calling this method.
		 * @throw ElementNotExist
		 */
		void remove() {
			if (last != -1){
				int tmp = u->remove(last);
				last = -1;
				if (tmp != -1) forgetMeNot.add(u->getElement(tmp));
			} else if (lastE){
				u->removeE(tmp);
				lastE = false;
			} else throw ElementNotExist();
		}
		
		bool LastE(){
			return lastE;
		}
		
	};

	/**
	 * TODO Constructs an empty priority queue.
	 */
	PriorityQueue() {
		seq.clear();
	}

	/**
	 * TODO Destructor
	 */
	~PriorityQueue() {
	}

	/**
	 * TODO Assignment operator
	 */
	PriorityQueue &operator=(const PriorityQueue &x) {
		if (&x == this) return *this;
		seq = x.getSeq();
		return *this;
	}

	/**
	 * TODO Copy-constructor
	 */
	PriorityQueue(const PriorityQueue &x) {
		seq = x.getSeq();
	}

	/**
	 * TODO Initializer_list-constructor
	 * Constructs a priority queue over the elements in this Array List.
	 * Requires to finish in O(n) time.
	 */
	PriorityQueue(const ArrayList<V> &x) {
		seq = x;
		for (int i = seq.size() / 2; i >= 0; --i) down(i);
	}

	/**
	 * TODO Returns an iterator over the elements in this priority queue.
	 */
	Iterator iterator() {
		Iterator t(this);
		return t;
	}

	/**
	 * TODO Removes all of the elements from this priority queue.
	 */
	void clear() {
		seq.clear();
	}

	/**
	 * TODO Returns a const reference to the front of the priority queue.
	 * If there are no elements, this function should throw ElementNotExist exception.
	 * @throw ElementNotExist
	 */
	const V &front() const {
		if (seq.isEmpty()) throw ElementNotExist();
		return seq.get(0);
	}

	/**
	 * TODO Returns true if this PriorityQueue contains no elements.
	 */
	bool empty() const {
		return seq.isEmpty();
	}

	/**
	 * TODO Add an element to the priority queue.
	 */
	void push(const V &value) {
		seq.add(value);
		up(seq.size() - 1);
	}

	/**
	 * TODO Removes the top element of this priority queue if present.
	 * If there is no element, throws ElementNotExist exception.
	 * @throw ElementNotExist
	 */
	void pop() {
		if (seq.isEmpty()) throw ElementNotExist();
		Swap(0, seq.size() - 1);
		seq.removeIndex(seq.size() - 1);
		down(0);
	}

	/**
	 * TODO Returns the number of key-value mappings in this map.
	 */
	int size() const {
		return seq.size();
	}
	
	const ArrayList<V>& getSeq()const{
		return seq;
	}
	
	V getElement(int t){
		return seq.get(t);
	}
};

#endif
