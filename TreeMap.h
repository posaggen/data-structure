/** @file */
#ifndef __TREEMAP_H
#define __TREEMAP_H

#include "ElementNotExist.h"

/**
 * TreeMap is the balanced-tree implementation of map. The iterators must
 * iterate through the map in the natural order (operator<) of the key.
 */
template<class K, class V>
class TreeMap {
private:
	
	class Node{
	public:
		K key;
		V value;
		int r;
		Node *ch[2], *f;
		Node(K _key, V _value, int _r){
			key = _key;
			value = _value;
			r = _r;
			ch[0] = ch[1] = f = NULL;
		}
	};
	
	Node *root;
	
	const static long long multiplier = 48271;
	const static long long M = 2147483647ll;
	
	long long seed;
	
	int total;
	
	long long Random(){
		seed = seed * multiplier % M;
		return seed;
	}
	
	void Rotate(Node *p, int w){
		Node *q = p->f, *y = q->f;
		p->f = y;
		if (y){
			if (y->ch[0] == q) y->ch[0] = p; else y->ch[1] = p;
		}
		y = p->ch[1 - w];
		if (y) y->f = q;
		q->ch[w] = y;
		q->f = p;
		p->ch[1 - w] = q;
	}
	
	void Splay(Node *p){
		while (p->f){
			Node *q = p->f, *y = q->f;
			if (y){
				if (y->ch[0] == q && q->ch[0] == p) Rotate(q, 0); else
					if (y->ch[1] == q && q->ch[1] == p) Rotate(q, 1); else 
						if (q->ch[0] == p) Rotate(p, 0); else Rotate(p, 1);
			}
			if (p->f->ch[0] == p) Rotate(p, 0); else Rotate(p, 1);
		}
		root = p;
	}
	
	void dfs(Node *u){
		if (!u) return;
		dfs(u->ch[0]);
		dfs(u->ch[1]);
		delete u;
	}
	
public:
	
	
	
	class Entry {
		K key;
		V value;
	public:
		Entry(K k, V v) {
			key = k;
			value = v;
		}

		const K& getKey() const {
			return key;
		}

		const V& getValue() const {
			return value;
		}
	};

	class Iterator {
	private:
		Node *now;
	public:
		Iterator(const TreeMap *t){
			now = t->root;
			if (now) while (now->ch[0]) now = now->ch[0];
		}
		/**
		 * TODO Returns true if the iteration has more elements.
		 */
		bool hasNext() {
			if (!now) return false; else return true;
		}

		/**
		 * TODO Returns the next element in the iteration.
		 * @throw ElementNotExist exception when hasNext() == false
		 */
		Entry next() {
			if (!hasNext()) throw ElementNotExist();
			Entry t = Entry(now->key, now->value);
			if (now->ch[1]){
				now = now->ch[1];
				while (now->ch[0]) now = now->ch[0];
			} else {
				Node *v = now;
				now = now->f;
				while (now && now->key < v->key){
					v = now;
					now = now->f;
				}
			}
			return t;
		}
	};

	/**
	 * TODO Constructs an empty tree map.
	 */
	TreeMap() {
		total = 0;
		root = NULL;
		seed = 10086;
	}

	/**
	 * TODO Destructor
	 */
	~TreeMap(){
		clear();
	}

	/**
	 * TODO Assignment operator
	 */
	TreeMap &operator=(const TreeMap &x) {
		if (&x == this) return *this;
		clear();
		total = x.size();
		seed = 10086;
		for (Iterator iter = x.iterator(); iter.hasNext();){
			Entry e = iter.next();
			
			K key = e.getKey();
			V value = e.getValue();
			if (!root){
				root = new Node(key, value, Random());
				continue;
			}
			Node *u = root, *f;
			bool found = false;
			while (u){
				f = u;
				if (u->key == key){
					u->value = value;
					found = true;
					break;
				} else if (u->key < key){
					u = u->ch[1];
				} else u = u->ch[0];
			}
			if (found) continue;
			Node *t = new Node(key, value, Random());
			t->f = f;
			if (t->key < f->key) f->ch[0] = t; else f->ch[1] = t;
			while (t->f && t->f->r < t->r){
				if (t->f->ch[0] == t) Rotate(t, 0); else Rotate(t, 1);
			}
			if (!t->f) root = t;
		}
		return *this;
	}

	/**
	 * TODO Copy-constructor
	 */
	TreeMap(const TreeMap &x) {
		total = x.size();
		seed = 10086;
		root = NULL;
		for (Iterator iter = x.iterator(); iter.hasNext();){
			Entry e = iter.next();
			
			K key = e.getKey();
			V value = e.getValue();
			if (!root){
				root = new Node(key, value, Random());
				continue;
			}
			Node *u = root, *f;
			bool found = false;
			while (u){
				f = u;
				if (u->key == key){
					u->value = value;
					found = true;
					break;
				} else if (u->key < key){
					u = u->ch[1];
				} else u = u->ch[0];
			}
			if (found) continue;
			Node *t = new Node(key, value, Random());
			t->f = f;
			if (t->key < f->key) f->ch[0] = t; else f->ch[1] = t;
			while (t->f && t->f->r < t->r){
				if (t->f->ch[0] == t) Rotate(t, 0); else Rotate(t, 1);
			}
			if (!t->f) root = t;
		}
	}

	/**
	 * TODO Returns an iterator over the elements in this map.
	 */
	Iterator iterator() const {
		Iterator t(this);
		return t;
	}

	/**
	 * TODO Removes all of the mappings from this map.
	 */
	void clear() {
		dfs(root);
		root = NULL;
		total = 0;
	}

	/**
	 * TODO Returns true if this map contains a mapping for the specified key.
	 */
	bool containsKey(const K &key) const {
		Node *u = root;
		while (u){
			if (u->key == key) {
				return true;
			}
			if (u->key < key) u = u->ch[1]; else u = u->ch[0];
		}
		return false;
	}

	/**
	 * TODO Returns true if this map maps one or more keys to the specified value.
	 */
	bool containsValue(const V &value) const {
		for (Iterator iter = iterator(); iter.hasNext();){
			Entry t = iter.next();
			if (t.getValue() == value) return true;
		}
		return false;
	}

	/**
	 * TODO Returns a const reference to the value to which the specified key is mapped.
	 * If the key is not present in this map, this function should throw ElementNotExist exception.
	 * @throw ElementNotExist
	 */
	const V &get(const K &key) const {
		Node *u = root;
		while (u){
			if (u->key == key) {
				return u->value;
			}
			if (u->key < key) u = u->ch[1]; else u = u->ch[0];
		}
		throw ElementNotExist();
	}

	/**
	 * TODO Returns true if this map contains no key-value mappings.
	 */
	bool isEmpty() const {
		if (total == 0) return true; else return false;
	}

	/**
	 * TODO Associates the specified value with the specified key in this map.
	 */
	void put(const K &key, const V &value) {
		if (!root){
			++total;
			root = new Node(key, value, Random());
			return;
		}
		Node *u = root, *f;
		while (u){
			f = u;
			if (u->key == key){
				u->value = value;
				return;
			} else if (u->key < key){
				u = u->ch[1];
			} else u = u->ch[0];
		}
		Node *t = new Node(key, value, Random());
		t->f = f;
		if (t->key < f->key) f->ch[0] = t; else f->ch[1] = t;
		++total;
		while (t->f && t->f->r < t->r){
			if (t->f->ch[0] == t) Rotate(t, 0); else Rotate(t, 1);
		}
		if (!t->f) root = t;
	}

	/**
	 * TODO Removes the mapping for the specified key from this map if present.
	 * If there is no mapping for the specified key, throws ElementNotExist exception.
	 * @throw ElementNotExist
	 */
	void remove(const K &key) {
		Node *u = root;
		while (u){
			if (u->key == key) break;
			if (u->key < key) u = u->ch[1]; else u = u->ch[0];
		}
		if (!u) throw ElementNotExist();
		--total;
		Splay(u);
		if (!u->ch[0]){
			if (!u->ch[1]){
				delete u;
				root = NULL;
				return;
			}
			root = u->ch[1];
			root->f = NULL;
			delete u;
		} else {
			Node *v = u->ch[0];
			while (v->ch[1]) v = v->ch[1];
			Splay(v);
			v->ch[1] = u->ch[1];
			if (u->ch[1]) u->ch[1]->f = v;
			v->f = NULL;
			delete u;
		}
	}

	/**
	 * TODO Returns the number of key-value mappings in this map.
	 */
	int size() const {
		return total;
	}
};

#endif
