#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <ctime> 
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <bitset>
#include "ElementNotExist.h"
#include "IndexOutOfBound.h"
#include "PriorityQueue.h"

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

void compare(PriorityQueue<int> Q, multiset<int> S){
	vector<int> V;
	for (auto iter = Q.iterator(); iter.hasNext(); ){
		int t = iter.next();
//cout << t << ' ';
		V.push_back(t);
	}
//cout << endl;
	sort(V.begin(), V.end());
	bool ok = true;
	multiset<int>::iterator iter = S.begin();
	for (int i = 0; i < (int)V.size(); ++i){
		//cout << V[i] << ' ' << *iter;
		if (V[i] != *iter){
			ok = false;
			//cout << ' ' << '!';
		}
		//cout << endl;
		++iter;
	}
	if (!ok) for(;;);
	if ((int)S.size() != (int)Q.size()) for(;;);
	for (auto iter = Q.iterator(); iter.hasNext(); ){
		iter.next();
		iter.remove();
	}
	if (Q.size()) for(;;);
}

int main(){
//	freopen("test.in", "r", stdin);
//	freopen("test.out", "w", stdout);
	srand(time(0));
	PriorityQueue<int> Q, PP;
	PP.clear();
	Q = PP;
	multiset<int> S;
	for (int i = 0; i < 100000; ++i){
		int t = rand();
		Q.push(t);
		S.insert(t);
	}
	PriorityQueue<int> B(Q), C;
	C = B;
	for (auto iter = B.iterator(); iter.hasNext(); ){
		iter.next();
		iter.remove();
	}
	compare(Q, S);
	compare(C, S);
	if (B.size()) for(;;);
//	for (auto iter = Q.iterator(); iter.hasNext(); ){
//		cout << iter.next() << ' ';
//	}
//	cout << endl;
	
	int tot = 0;
	for (auto iter = Q.iterator(); iter.hasNext(); ){
		int t = iter.next();
//cout << t;
		
		if (rand() % 9 != 0 && !S.empty()){
//cout << '!';
			iter.remove();
			S.erase(S.lower_bound(t));
			++tot;
		}
		if (Q.front() != *S.begin()) for(;;);
		if ((int)S.size() != (int)Q.size()) for(;;);
//cout << ' ';
	}
//cout << endl;
	compare(Q, S);
	return 0;
}


